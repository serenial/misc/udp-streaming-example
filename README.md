# UDP Streaming Example

Some working but not production ready code to demonstrate streaming to multiple clients over UDP.

## Operation
The UDP streamer acts as a host to which clients can connect. The initial connection is established by TCP to allow for UDP port information to be exchanged. A regular TCP heartbeat is also sent out to check that the client is still active.

In order to overcome the limit of the 548 byte packetsize, larger datasets are split into chunks with the following layout:

| ID(U32) | TOTAL_CHUNKS(I32) | CHUNK_NUM(I32) | LENGTH(I16) | <DATA> |

As chunks can arrive out of sequence, enough information is provided to allow for datasets to be reconstructed.

The UDP Streamer provides a control to specify how often a packet should be sent and also the size in bytes of each dataset.

The UDP Stream Reader connects to the UDP Streamer and starts receiving data. It reconstructs chunks by storing them in a buffer which is cleared when a complete dataset is ready.

There is no way to recover lost chunks - so the UDP Stream Reader will just wait until it receives a new dataset.

The Stream Reader provides a control to specify how many dropped chunks are allowable whilst waiting to construct a complete dataset.

## Dependencies
LabVIEW 2014